package fr.mindstorm38.byteserial;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class VariableByteArray {
	
	private static final Map<Class<? extends Object>, ByteSerial> serializers = new HashMap<Class<? extends Object>, ByteSerial>();
	
	public static void registerSerializers(Class<? extends Object> clazz, ByteSerial serializer) {
		VariableByteArray.serializers.put(clazz, serializer);
	}
	
	public static ByteSerial getSerializer(Class<? extends Object> clazz) {
		
		ByteSerial byteSerial = null;
		Class<?> actualClass = clazz;
		
		while (byteSerial == null) {
			
			byteSerial = VariableByteArray.serializers.get( actualClass );
			
			if (byteSerial != null) break;
			
			actualClass = actualClass.getSuperclass();
			
			if (actualClass == null) return null;
			
		}
		
		if (byteSerial != null) { // Caching
			VariableByteArray.serializers.put(actualClass, byteSerial);
		}
		
		return byteSerial;
		
	}
	
	public static VariableByteArray read(File file) throws IOException {
		return new VariableByteArray( IOUtils.getBytesFromFile(file) );
	}
	
	public static void write(File file, VariableByteArray array) throws IOException {
		IOUtils.writeBytesToFile(file, array.getBytes());
	}
	
	private byte[] bytes = null;
	private int readIndex = 0;
	
	public VariableByteArray(byte[] array) {
		this.bytes = array;
	}
	
	public VariableByteArray() {
		this(new byte[0]);
	}
	
	public byte[] getBytes() {
		return this.bytes;
	}
	
	public int size() {
		return this.bytes.length;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (byte bt : this.bytes) {
			b.append(String.format("%02X ", bt));
			b.append(' ');
		}
		return b.toString();
	}
	
	// WRITE \\
	
	public VariableByteArray writeBytes(byte[] bytes) {
		byte[] newBytes = Arrays.copyOf(this.bytes, this.bytes.length + bytes.length);
		for (int i = 0; i < bytes.length; i++) newBytes[ this.bytes.length + i ] = bytes[ i ];
		this.bytes = newBytes;
		return this;
	}
	
	public VariableByteArray writeByte(byte byt) {
		byte[] newBytes = Arrays.copyOf(this.bytes, this.bytes.length + 1);
		newBytes[ newBytes.length - 1 ] = byt;
		this.bytes = newBytes;
		return this;
	}
	
	public VariableByteArray writeShort(short shrt) {
		return this.writeBytes(new byte[]{
				(byte) ( shrt >> 8 ),
				(byte) ( shrt >> 0 )
		});
	}
	
	public VariableByteArray writeInteger(int integer) {
		return this.writeBytes(new byte[]{
				(byte) ( integer >> 24 ),
				(byte) ( integer >> 16 ),
				(byte) ( integer >> 8 ),
				(byte) ( integer >> 0 )
		});
	}
	
	public VariableByteArray writeLong(long lng) {
		return this.writeBytes(new byte[]{
				(byte) ( lng >> 56 ),
				(byte) ( lng >> 48  ),
				(byte) ( lng >> 40 ),
				(byte) ( lng >> 32 ),
				(byte) ( lng >> 24 ),
				(byte) ( lng >> 16 ),
				(byte) ( lng >> 8 ),
				(byte) ( lng >> 0 ),
		});
	}
	
	public VariableByteArray writeFloat(float flt) {
		return this.writeInteger( Float.floatToRawIntBits(flt) );
	}
	
	public VariableByteArray writeDouble(double dbl) {
		return this.writeLong( Double.doubleToRawLongBits(dbl) );
	}
	
	public VariableByteArray writeString(String str) {
		byte[] bytes = str.getBytes( Charset.forName("UTF-8") );
		this.writeInteger( bytes.length );
		return this.writeBytes( bytes );
	}
	
	public VariableByteArray writeByteSerialObject(Object object, Class<?> clazz) {
		if (clazz == null) return this.writeByteSerialObject(object);
		ByteSerial byteserial = VariableByteArray.getSerializer(clazz);
		VariableByteArray vba = new VariableByteArray();
		byteserial.write(vba, object);
		return this.writeVariableByteArray(vba);
	}
	
	public VariableByteArray writeByteSerialObject(Object object) {
		Class<?> clazz = object.getClass();
		ByteSerial byteserial = VariableByteArray.getSerializer(clazz);
		VariableByteArray vba = new VariableByteArray();
		byteserial.write(vba, object);
		this.writeClass(clazz);
		return this.writeVariableByteArray(vba);
	}
	
	public VariableByteArray writeVariableByteArray(VariableByteArray vba) {
		byte[] bytes = vba.getBytes();
		this.writeInteger( bytes.length );
		return this.writeBytes( bytes );
	}
	
	public VariableByteArray writeClass(Class<?> clazz) {
		return this.writeString(clazz.getName());
	}
	
	public VariableByteArray writeUuid(UUID uuid) {
		this.writeLong( uuid.getMostSignificantBits() );
		return this.writeLong( uuid.getLeastSignificantBits() );
	}
	
	public VariableByteArray writeChar(char chr) {
		return this.writeString(String.valueOf(chr));
	}
	
	public VariableByteArray writeBoolean(boolean bool) {
		return this.writeByte( (byte) (bool ? 0x1 : 0x0) );
	}
	
	public VariableByteArray writeEnumConstant(Enum<?> enumeration) {
		return this.writeString(enumeration.name());
	}
	
	public <C> VariableByteArray writeCollection(Collection<? extends C> list, ArrayAcceptor<C> acceptor) {
		VariableByteArray indepedentArray = new VariableByteArray();
		int size = 0;
		for (C elt : list) {
			VariableByteArray acceptResult = acceptor.accept(elt);
			if (acceptResult != null) {
				indepedentArray.writeVariableByteArray(acceptResult);
				size++;
			}
		}
		this.writeInteger( size );
		return this.writeBytes( indepedentArray.getBytes() );
	}
	
	public <K, V> VariableByteArray writeMap(Map<K, V> map, ArrayAcceptor<K> keyAcceptor, ArrayAcceptor<V> valueAcceptor) {
		VariableByteArray indepedentArray = new VariableByteArray();
		indepedentArray.writeCollection( map.keySet(), keyAcceptor );
		indepedentArray.writeCollection( map.values(), valueAcceptor );
		return this.writeBytes( indepedentArray.getBytes() );
	}
	
	public VariableByteArray deleteBytes(int offset, int length) {
		this.bytes = Arrays.copyOfRange(this.bytes, offset, offset + length);
		return this;
	}
	
	public VariableByteArray deleteLastBytes(int number) {
		return this.deleteBytes(this.bytes.length - number, number - 1);
	}
	
	public VariableByteArray deleteLastByte() {
		return this.deleteLastBytes(1);
	}
	
	// READ \\
	
	public int getReadIndex() {
		return this.readIndex;
	}
	
	public VariableByteArray setReadIndex(int readIndex) {
		this.readIndex = readIndex;
		return this;
	}
	
	private void increaseReadIndex() {
		this.increaseReadIndex(1);
	}
	
	private void increaseReadIndex(int number) {
		this.readIndex += number;
		if ( this.readIndex > ( this.bytes.length ) ) this.throwOutOfBounds();
	}
	
	private void throwOutOfBounds() {
		throw new ArrayIndexOutOfBoundsException("Read Index out of bounds exception : " + this.readIndex);
	}
	
	public byte[] readBytes(int count) {
		this.increaseReadIndex(count);
		return Arrays.copyOfRange(this.bytes, this.readIndex - count, this.readIndex);
	}
	
	public byte readByte() {
		this.increaseReadIndex();
		return this.bytes[this.readIndex - 1];
	}
	
	public short readShort() {
		this.increaseReadIndex(2);
		return (short) (
				( this.bytes[this.readIndex - 2] ) << 8 |
				( this.bytes[this.readIndex - 1] & 0xFF )
		);
	}
	
	public int readInteger() {
		this.increaseReadIndex(4);
		return 
				( this.bytes[this.readIndex - 4] ) << 24 |
				( this.bytes[this.readIndex - 3] & 0xFF ) << 16 |
				( this.bytes[this.readIndex - 2] & 0xFF ) << 8 |
				( this.bytes[this.readIndex - 1] & 0xFF )
		;
	}
	
	public long readLong() {
		this.increaseReadIndex(8);
		return (long) (
				( (long) this.bytes[this.readIndex - 8] ) << 56 |
				( (long) this.bytes[this.readIndex - 7] & 0xFF ) << 48 |
				( (long) this.bytes[this.readIndex - 6] & 0xFF ) << 40 |
				( (long) this.bytes[this.readIndex - 5] & 0xFF ) << 32 |
				( (long) this.bytes[this.readIndex - 4] & 0xFF ) << 24 |
				( (long) this.bytes[this.readIndex - 3] & 0xFF ) << 16 |
				( (long) this.bytes[this.readIndex - 2] & 0xFF ) << 8 |
				( (long) this.bytes[this.readIndex - 1] & 0xFF )
		);
	}
	
	public float readFloat() {
		return Float.intBitsToFloat( this.readInteger() );
	}
	
	public double readDouble() {
		return Double.longBitsToDouble( this.readLong() );
	}
	
	public String readString() {
		int length = this.readInteger();
		return new String( this.readBytes(length), Charset.forName("UTF-8") );
	}
	
	public Object readByteSerialObject(Class<?> clazz) throws ClassNotFoundException {
		if (clazz == null) return this.readByteSerialObject();
		ByteSerial byteserial = VariableByteArray.getSerializer(clazz);
		VariableByteArray vba = this.readVariableByteArray();
		return byteserial.read(vba);
	}
	
	public Object readByteSerialObject() throws ClassNotFoundException {
		Class<?> clazz = this.readClass();
		ByteSerial byteserial = VariableByteArray.getSerializer(clazz);
		VariableByteArray vba = this.readVariableByteArray();
		return byteserial.read(vba);
	}
	
	public VariableByteArray readVariableByteArray() {
		int length = this.readInteger();
		return new VariableByteArray( this.readBytes(length) );
	}
	
	public Class<?> readClass() throws ClassNotFoundException {
		String className = this.readString();
		return Class.forName(className);
	}
	
	public UUID readUuid() {
		Long most = this.readLong();
		Long least = this.readLong();
		return new UUID(most, least);
	}
	
	public char readChar() {
		return this.readString().charAt(0);
	}
	
	public boolean readBoolean() {
		return this.readByte() == 0x1;
	}
	
	public Enum<?> readEnumConstant(Class<? extends Enum<?>> enumClazz) {
		try {
			String name = this.readString();
			for (Field enumField : enumClazz.getDeclaredFields()) {
				if (enumField.isEnumConstant() && enumField.getName().equals(name)) {
					Object obj = enumField.get(enumClazz);
					if (obj instanceof Enum<?>) {
						return (Enum<?>) obj;
					}
				}
			}
		} catch (Exception e) {}
		return null;
	}
	
	public <C> Collection<C> readCollection(ArrayAcceptor<C> acceptor) {
		List<C> list = new ArrayList<C>();
		int size = this.readInteger();
		for (int i = 0; i < size; i++) {
			VariableByteArray vba = this.readVariableByteArray();
			C acceptorResult = acceptor.accept(vba);
			if (acceptorResult != null) list.add(acceptorResult);
		}
		return list;
	}
	
	public <K, V> Map<K, V> readMap(ArrayAcceptor<K> keyAcceptor, ArrayAcceptor<V> valueAcceptor) {
		Map<K, V> map = new HashMap<K, V>();
		Iterator<K> keys = this.readCollection(keyAcceptor).iterator();
		Iterator<V> values = this.readCollection(valueAcceptor).iterator();
		while (keys.hasNext() && values.hasNext()) {
			K key = keys.next();
			V value = values.next();
			map.put(key, value);
		}
		return map;
	}
	
}
