package fr.mindstorm38.byteserial;

import java.util.UUID;

public interface ArrayAcceptor<C> {
	
	public static final ArrayAcceptor<String> ACCEPTOR_STRING					 = new ArrayAcceptor<String>() {
		public VariableByteArray accept(String elt) {
			return new VariableByteArray().writeString(elt);
		}
		public String accept(VariableByteArray arr) {
			return arr.readString();
		}
	};
	
	public static final ArrayAcceptor<Byte> ACCEPTOR_BYTE						= new ArrayAcceptor<Byte>() {
		public VariableByteArray accept(Byte elt) {
			return new VariableByteArray().writeByte(elt);
		}
		public Byte accept(VariableByteArray arr) {
			return arr.readByte();
		}
	};
	
	public static final ArrayAcceptor<Short> ACCEPTOR_SHORT						= new ArrayAcceptor<Short>() {
		public VariableByteArray accept(Short elt) {
			return new VariableByteArray().writeShort(elt);
		}
		public Short accept(VariableByteArray arr) {
			return arr.readShort();
		}
	};
	
	public static final ArrayAcceptor<Integer> ACCEPTOR_INTEGER					= new ArrayAcceptor<Integer>() {
		public VariableByteArray accept(Integer elt) {
			return new VariableByteArray().writeInteger(elt);
		}
		public Integer accept(VariableByteArray arr) {
			return arr.readInteger();
		}
	};
	
	public static final ArrayAcceptor<Long> ACCEPTOR_LONG						= new ArrayAcceptor<Long>() {
		public VariableByteArray accept(Long elt) {
			return new VariableByteArray().writeLong(elt);
		}
		public Long accept(VariableByteArray arr) {
			return arr.readLong();
		}
	};

	public static final ArrayAcceptor<Float> ACCEPTOR_FLOAT						= new ArrayAcceptor<Float>() {
		public VariableByteArray accept(Float elt) {
			return new VariableByteArray().writeFloat(elt);
		}
		public Float accept(VariableByteArray arr) {
			return arr.readFloat();
		}
	};
	
	public static final ArrayAcceptor<Double> ACCEPTOR_DOUBLE					= new ArrayAcceptor<Double>() {
		public VariableByteArray accept(Double elt) {
			return new VariableByteArray().writeDouble(elt);
		}
		public Double accept(VariableByteArray arr) {
			return arr.readDouble();
		}
	};
	
	public static final ArrayAcceptor<UUID> ACCEPTOR_UUID						= new ArrayAcceptor<UUID>() {
		public VariableByteArray accept(UUID elt) {
			return new VariableByteArray().writeUuid(elt);
		}
		public UUID accept(VariableByteArray arr) {
			return arr.readUuid();
		}
	};
	
	public static final ArrayAcceptor<Boolean> ACCEPTOR_BOOLEAN					= new ArrayAcceptor<Boolean>() {
		public VariableByteArray accept(Boolean elt) {
			return new VariableByteArray().writeBoolean(elt);
		}
		public Boolean accept(VariableByteArray arr) {
			return arr.readBoolean();
		}
	};
	
	public static final ArrayAcceptor<Character> ACCEPTOR_CHAR					= new ArrayAcceptor<Character>() {
		public VariableByteArray accept(Character elt) {
			return new VariableByteArray().writeChar(elt);
		}
		public Character accept(VariableByteArray arr) {
			return arr.readChar();
		}
	};
	
	public static final ArrayAcceptor<Object> ACCEPTOR_BYTE_SERIAL				= new ArrayAcceptor<Object>() {
		public VariableByteArray accept(Object elt) {
			return new VariableByteArray().writeByteSerialObject(elt);
		}
		public Object accept(VariableByteArray arr) {
			try { return arr.readByteSerialObject(); } catch (ClassNotFoundException e) { return null; }
		}
	};
	
	public static final ArrayAcceptor<Enum<?>> ACCEPTOR_ENUM_CONSTANT			= new ArrayAcceptor<Enum<?>>() {
		public VariableByteArray accept(Enum<?> elt) {
			return new VariableByteArray().writeClass(elt.getDeclaringClass()).writeEnumConstant(elt);
		}
		@SuppressWarnings("unchecked")
		public Enum<?> accept(VariableByteArray arr) {
			try {
				Class<?> clazz = arr.readClass();
				if (!Enum.class.isAssignableFrom(clazz)) return null;
				return arr.readEnumConstant((Class<? extends Enum<?>>) clazz);
			} catch  (Exception e) {
				return null;
			}
		}
	};
	
	// --- \\
	
	public VariableByteArray accept(C elt);
	public C accept(VariableByteArray arr);
	
}
