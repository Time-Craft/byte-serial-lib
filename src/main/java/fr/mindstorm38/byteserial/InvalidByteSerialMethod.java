package fr.mindstorm38.byteserial;

public class InvalidByteSerialMethod extends RuntimeException {
	
	private static final long serialVersionUID = -8615735475715863250L;
	
	public InvalidByteSerialMethod(String m) {
		super(m);
	}
	
	public InvalidByteSerialMethod(Throwable t) {
		super(t);
	}
	
	public InvalidByteSerialMethod(String m, Throwable t) {
		super(m, t);
	}
	
	public InvalidByteSerialMethod() {
		super();
	}
	
}
