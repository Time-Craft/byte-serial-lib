package fr.mindstorm38.byteserial;

import java.io.File;
import java.io.IOException;

public interface ByteSerial {
	
	public void write(VariableByteArray array, Object obj);
	
	public Object read(VariableByteArray array);
	
	public static void write(File file, Object obj, Class<?> clazz) throws IOException {
		VariableByteArray array = new VariableByteArray();
		array.writeByteSerialObject(obj, clazz);
		VariableByteArray.write(file, array);
	}
	
	public static void write(File file, Object obj) throws IOException {
		ByteSerial.write(file, obj, null);
	}
	
	public static Object read(File file, Class<?> clazz) throws IOException, ClassNotFoundException {
		return VariableByteArray.read(file).readByteSerialObject(clazz);
	}
	
	public static Object read(File file) throws IOException, ClassNotFoundException {
		return ByteSerial.read(file, null);
	}
	
}